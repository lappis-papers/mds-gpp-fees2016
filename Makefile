all: bib tex
	pdflatex mds-gpp-fees2016.tex
	evince mds-gpp-fees2016.pdf &

bib: mds-gpp-fees2016.bib
	pdflatex mds-gpp-fees2016.tex
	bibtex mds-gpp-fees2016

tex: mds-gpp-fees2016.tex
	pdflatex mds-gpp-fees2016.tex

clean:
	rm *.bbl *.aux *.blg *.log *.pdf
